from pyramid.security import Allow, Everyone


# The context for our view has an access control list
# The edit permission is available to the group:editors principal
class Root(object):
    __acl__ = [(Allow, Everyone, 'view'),
               (Allow, 'group:editors', 'edit')]

    def __init__(self, request):
        pass