from pyramid.view import (
    view_config,
    view_defaults
    )

# View class to replace the functions
# Since both methods use the same template, we can use the defaults decorator
@view_defaults(renderer='home.pt')
class TutorialViews:
    def __init__(self, request):
        self.request = request

    @view_config(route_name='home')
    def home(self):
        return {'name': 'Home View'}

    @view_config(route_name='hello')
    def hello(self):
        return {'name': 'Hello View'}