cd ~
mkdir -p projects/quick_tutorial
cd projects/quick_tutorial
export VENV=~/projects/quick_tutorial/env
python3 -m venv $VENV
$VENV/bin/pip install --upgrade pip setuptools
$VENV/bin/pip install "pyramid==1.9.2" waitress